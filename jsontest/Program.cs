﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using libjson;

namespace jsontest {
	class Program {
		static void Main(string[] args) {
			JSON j = JSON.Parse(File.ReadAllText("testdata.json"));
			Console.WriteLine("session_uid : " + j["session_uid"]);
			Console.WriteLine("code : " + j["code"]);
			Console.WriteLine("message : " + j["message"]);
		}
	}
}
